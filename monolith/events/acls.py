from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_location_photo(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    location = city + ', ' + state
    response = requests.get('https://api.pexels.com/v1/search', params=location, headers=headers)
   # picture = json.loads(response.content)
   # result = {"picture_url": response.url}

    return response.url

def get_weather_data(city, state):
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(geo_url)
    geo = json.loads(response.content)
    lat = geo[0].get("lat")
    lon = geo[0].get("lon")

    weather_map_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_map_url)
    parsed_weather_response = json.loads(weather_response.content)
    main = parsed_weather_response["main"]
    weather = parsed_weather_response["weather"]
    result = {
        "temp": main["temp"],
        "description": weather[0]["description"]
    }

    return result

print(get_weather_data('san antonio', 'TX'))
